package auts.fpay.android.testcases;

import auts.fpay.android.pom.page.FormularioCrearCuenta;
import auts.fpay.android.pom.page.HomePage;
import auts.fpay.android.testcases.TestBase;

import genericos.funciones.AllureListener;
import org.springframework.context.annotation.Description;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Test de apoyo para appium.
 * Libreria Test: TESTNG
 * @author eduardo.araya
 */
@Listeners({AllureListener.class})
public class atc01_ingresoFormularioCrearCuenta extends TestBase {

    HomePage fpayHomePage;
    FormularioCrearCuenta formularioCrearCuenta;

    @Test
    @Description("Test que valida el correcto llenado de el formulario de Creacion de Cuenta en Sistema FPAY")
    public void ingresoFormularioCrearCuenta(){
        fpayHomePage = new HomePage(driver);
        fpayHomePage.clickBotonInicial();
        fpayHomePage.clickBotonCrearCuenta();
        fpayHomePage.ingresarRut("154372202");
        formularioCrearCuenta = new FormularioCrearCuenta(driver);
        formularioCrearCuenta.rellenarFormulario("eduardo","araya","fahelo@gmail.com","971411165");
        Assert.assertTrue(true);
    }

}
