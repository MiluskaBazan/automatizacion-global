package auts.jetsmart.web.testcases;

import auts.jetsmart.web.pom.pages.JetSmartHomePage;
import auts.jetsmart.web.pom.pages.JetSmartInicioSesionPage;
import io.github.bonigarcia.seljup.SeleniumJupiter;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import genericos.funciones.LoadProperties;

import java.util.Properties;

@ExtendWith(SeleniumJupiter.class)
public class atc01_InicioSesionErroneo extends TestBase {

   protected JetSmartHomePage jsHomePage;
   protected JetSmartInicioSesionPage jsInicioSesionPage;

    @Test
    public void atc01_InicioSesionErroneo() throws InterruptedException {
        jsHomePage = new JetSmartHomePage(driver);
        Properties properties = LoadProperties.loadProperties(appProperties);
        jsHomePage.goToUrl(properties.getProperty("url_home"));
        jsHomePage.CerrarPopUp();
        jsHomePage.ClickIniciaSesion();
        //InicioSesion Page
        jsInicioSesionPage = new JetSmartInicioSesionPage(driver);
        jsInicioSesionPage.IniciarSesionPersonas("userTest","userPass");
    }

}
