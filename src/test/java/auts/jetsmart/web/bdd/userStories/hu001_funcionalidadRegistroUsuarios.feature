Feature: Sistema de Registro de Usuarios Ecommerce
  Se describen los escenarios que se esperan cubra la funcioanlidad de registro
  de clientes del sistema Ecommerce

  @test-reader
  Scenario: Obtener Formulario Ingreso de Cliente desde HomePage
    Given ingreso como usuario desde el homePage
    When presiono el boton de Sign-In
    Then puedo comenzar el proceso de ingreso de Cliente

  @test-reader
  Scenario: Ingreso Cliente
    Given que como usuario estoy en la web de ingreso de cliente
    When  ingreso mis datos
      |Email address|Password|
    And presiono Sign In
    Then puedo ingresar como cliente