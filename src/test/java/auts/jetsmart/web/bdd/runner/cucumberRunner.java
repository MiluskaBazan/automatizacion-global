package auts.jetsmart.web.bdd.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/auts/jetsmart/web/bdd/userStories",
        glue = "auts.jetsmart.web.bdd.steps" ,
        tags = "@test-reader",
        stepNotifications = true)
public class cucumberRunner {
}
