package auts.jetsmart.web.pom.pages;

import auts.jetsmart.web.pom.base.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * URL: https://jetsmart.com/cl/es/
 * HomePage para automatizacion de casos de Pruebas
 */
public class JetSmartHomePage extends SeleniumBase {

    public JetSmartHomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    //Identificacion de Objetos
    private By aTagCheckIn = By.xpath("//a[@href='#dg-check-in-modal']");
    private By aTagAdministraTuViaje = By.xpath("//a[@href='#dg-administer-modal']");
    private By aTagEmpresas = By.xpath("//a[@href='./empresas']");
    private By aTagIniciaSesion = By.cssSelector(".login-container > li:nth-child(4) > a:nth-child(1)");
    private By aTagClienteBancoEstado = By.cssSelector(".login-container > li:nth-child(5) > a:nth-child(1)");
    private By btnPopUp = By.xpath("//*[@id='onesignal-slidedown-cancel-button']");



    //Metodos Keyword Driven:  JetSmartHomePage
    public void ClickCheckIn(){
        click(aTagCheckIn);
    }
    public void ClickAdministraTuViaje(){
        click(aTagAdministraTuViaje);
    }
    public void ClickEmpresas(){
        click(aTagEmpresas);
    }
    public void ClickIniciaSesion(){
        click(aTagIniciaSesion);
    }
    public void ClickClienteBancoEstado(){
        click(aTagClienteBancoEstado);
    }

    public void CerrarPopUp() throws InterruptedException {
        if(isDisplayed(btnPopUp)){
            click(btnPopUp);
        }
    }






}
