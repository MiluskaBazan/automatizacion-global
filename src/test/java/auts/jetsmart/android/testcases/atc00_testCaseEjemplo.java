package auts.jetsmart.android.testcases;

import auts.parquearauco.web.testcases.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

/**
 * Test de apoyo para appium.
 * Libreria Test: TESTNG
 * @author eduardo.araya
 */
public class atc00_testCaseEjemplo extends TestBase {

    @Test
    public void TestOne(){
        String url = "http://www.google.com";
        driver.get(url);
        driver.findElement(By.name("q")).sendKeys("automation");
        driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
        System.out.println("Completed test One");
    }

}
