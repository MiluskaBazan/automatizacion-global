package auts.parquearauco.webandroid.testcases;

import genericos.funciones.AllureListener;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Test de apoyo para appium.
 * Libreria Test: TESTNG
 * @author eduardo.araya
 */
@Listeners({AllureListener.class})
public class atc01_busquedaporGoogle extends TestBase{

    @Test
    public void busquedaenChromeAndroid() throws InterruptedException {
        String url = "http://www.google.com";
        driver.get(url);
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("body > div:nth-child(4) > div > mobile-promo > div > div > div > div.KaTqAd > g-flat-button")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#tsf > div:nth-child(1) > div.A7Yvie.emca > div.zGVn2e > button.Tg7LZd.search_button_suggest > div > span > svg")).click();
        Thread.sleep(2000);
        driver.findElement(By.name("q")).sendKeys("parquearauco");
        Thread.sleep(2000);
        driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        System.out.println("Test: Busqueda Parque Arauco Chrome Android. Completado");
    }

}
