package auts.parquearauco.webandroid.testcases;

import auts.parquearauco.web.pom.pages.ParqueAraucoHomePage;
import genericos.funciones.AllureListener;
import genericos.funciones.LoadProperties;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Properties;

/**
 * Test de apoyo para appium.
 * Libreria Test: TESTNG
 * @author eduardo.araya
 */
@Listeners({AllureListener.class})
public class atc02_busquedaChromeAndroid extends TestBase{

    protected ParqueAraucoHomePage PAHomePage;
    protected ParqueAraucoHomePage PAInicioSesionPage;

    @Test
    public void busquedaenChromeAndroid() throws InterruptedException {

        PAHomePage = new ParqueAraucoHomePage(driver);
        PAHomePage.goToUrl(properties.getProperty("url_home"));
        //InicioSesion Page
        PAInicioSesionPage = new ParqueAraucoHomePage(driver);
        Thread.sleep(3000);
        //Ajstes mobile
        PAInicioSesionPage.cerrarInformacionHome();
        PAInicioSesionPage.irAServicios();
        Thread.sleep(3000);
        Assert.assertTrue(true);
    }

}
