package auts.parquearauco.webandroid.testcases;

import genericos.funciones.AllureListener;
import genericos.funciones.LoadProperties;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

@Listeners({AllureListener.class})
public class TestBase {

    protected static AppiumDriver<MobileElement> driver;
    protected static String appProperties= "auts\\parquearauco";
    Properties properties = LoadProperties.loadProperties(appProperties);

    @BeforeTest
    public void setup() {

        properties = LoadProperties.loadProperties("auts\\parquearauco");

        try {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-geolocation");
            options.addArguments("disable-infobars");
            //Capabilities Basicos
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.PLATFORM_NAME,"ANDROID");
            caps.setCapability(MobileCapabilityType.VERSION,"9");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME,"faheloAndroid");
            caps.setCapability(MobileCapabilityType.UDID,"emulator-5554");
            caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,60);
            caps.setCapability("chromedriverExecutable",properties.getProperty("chromedriverExecutable_path"));
            caps.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
            caps.setCapability(ChromeOptions.CAPABILITY,options);


            //caps.setCapability(MobileCapabilityType.APP,fPayProperties.getProperty("apk_path")); //filename

            //URL Appium
            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            //inicializamos el driver de appium
            driver = new AppiumDriver<MobileElement>(url,caps);
            //driver = new AndroidDriver<MobileElement>(url,caps);
            //driver = new IOSDriver<MobileElement>(url,caps);


        } catch (MalformedURLException e) {
            System.out.println("Causa: "+ e.getCause());
            System.out.println("Mensaje: " +e.getMessage());
            e.printStackTrace();
        }


    }

    @AfterTest
    public void teardown(){
        driver.close();
        driver.quit();


    }
}
