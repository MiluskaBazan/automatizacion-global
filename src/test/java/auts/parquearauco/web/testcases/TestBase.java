package auts.parquearauco.web.testcases;


import genericos.funciones.AllureListener;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

@Listeners({AllureListener.class})
public class TestBase {

    public WebDriver driver;
    public static ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();
    protected static String appProperties= "auts\\parquearauco";

    public static synchronized WebDriver getDriver() {
        return tdriver.get();
    }

    @BeforeTest
    public void SetUp(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        tdriver.set(driver);
    }


    @AfterTest
    public void Close(){
        driver.close();
    }


}
