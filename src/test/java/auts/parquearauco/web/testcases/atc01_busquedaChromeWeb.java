package auts.parquearauco.web.testcases;


import auts.parquearauco.web.pom.pages.ParqueAraucoHomePage;
import genericos.funciones.AllureListener;
import genericos.funciones.LoadProperties;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Properties;


@Listeners({AllureListener.class})
public class atc01_busquedaChromeWeb extends TestBase {

   private ParqueAraucoHomePage PAHomePage;
   private ParqueAraucoHomePage PAInicioSesionPage;

    @Test(description = "validar buscador HomePage")@Severity(SeverityLevel.NORMAL)@Description("Test Case 01: Web")@Story("User Stories 01: Buscador Web")
    public void atc01_busquedaChromeWeb() throws InterruptedException {
        PAHomePage = new ParqueAraucoHomePage(driver);
        Properties properties = LoadProperties.loadProperties(appProperties);
        PAHomePage.goToUrl(properties.getProperty("url_home"));

        //InicioSesion Page
        PAInicioSesionPage = new ParqueAraucoHomePage(driver);
        Thread.sleep(3000);
        AllureListener.saveScreenShot(driver);
        PAInicioSesionPage.cerrarInformacionHome();
        AllureListener.saveScreenShot(driver);
        PAInicioSesionPage.irAServicios();
        Thread.sleep(3000);
        AllureListener.saveScreenShot(driver);
        Assert.assertTrue(false);
    }



}
