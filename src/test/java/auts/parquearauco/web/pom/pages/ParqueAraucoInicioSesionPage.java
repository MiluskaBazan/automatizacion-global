package auts.parquearauco.web.pom.pages;

import auts.jetsmart.web.pom.base.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


/**
 * Page Url: https://booking.jetsmart.com/V2/Login
 */
public class ParqueAraucoInicioSesionPage extends SeleniumBase {

    public ParqueAraucoInicioSesionPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    //Identificacion de Objetos
    private By seleccionPersonas = By.cssSelector("#memberLoginForm > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > label:nth-child(1) > span:nth-child(2)");



}
