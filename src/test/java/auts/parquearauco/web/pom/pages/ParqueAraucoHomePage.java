package auts.parquearauco.web.pom.pages;

import auts.jetsmart.web.pom.base.SeleniumBase;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * URL: https://www.parquearauco.cl/
 * HomePage para automatizacion de casos de Pruebas
 */
public class ParqueAraucoHomePage extends SeleniumBase {

    public ParqueAraucoHomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    //Identificacion de Objetos
    private By infoCloseButton = By.cssSelector("#myModal > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)");
    private By lblInicio = By.xpath("//span[contains(text(),'Inicio')]");
    private By lblTiendas = By.xpath("//span[contains(text(),'Tiendas')]");
    private By lblPanoramas = By.xpath("//span[contains(text(),'Panoramas')]");
    private By lblGastronomía = By.xpath("//span[contains(text(),'Gastronomía')]");
    private By lblServicios = By.xpath("//span[contains(text(),'Servicios')]");
    private By lblBlog = By.xpath("//span[contains(text(),'Blog')]");
    private By lblhorarios = By.xpath("//span[@class='horarios-page']");
    private By searchBar = By.xpath("//img[@id='searchBar']");
    private By burgerIMG = By.xpath("//img[@src='https://parquearauco.modyocdn.com/uploads/4c44c2c8-6e16-4030-ae81-d8c773e91d54/original/menu-icon.svg']");


    public void cerrarInformacionHome(){
        if(isDisplayed(infoCloseButton)){
            click(infoCloseButton);
        }
    }

    public void irAInicio(){
        if(isDisplayed(lblInicio)){
            click(lblInicio);
        }
    }
    public void irATiendas(){
        if(isDisplayed(lblTiendas)){
            click(lblTiendas);
        }
    }
    public void irAPanoramas(){
        if(isDisplayed(lblPanoramas)){
            click(lblPanoramas);
        }
    }
    public void irAGastronomia(){
        if(isDisplayed(lblGastronomía)){
            click(lblGastronomía);
        }
    }
    @Step("click a la pestaña Servicios")
    public void irAServicios(){
        if(isDisplayed(lblServicios)){
            click(lblServicios);
        }
    }


}
