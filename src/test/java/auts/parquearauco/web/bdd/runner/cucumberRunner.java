package auts.parquearauco.web.bdd.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/auts/parquearauco/web/bdd/userStories",
        glue = "auts.parquearauco.web.bdd.steps",
        strict = true,
        tags = "@test-parque",
        stepNotifications = true,
        plugin = {"pretty", "io.qameta.allure.cucumber5jvm.AllureCucumber5Jvm"})
public class cucumberRunner {
}
