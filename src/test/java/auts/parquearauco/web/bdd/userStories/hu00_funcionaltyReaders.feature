Feature: Tests Readers

  @test-1 @test-reader
  Scenario: Test 1
    Given ingresa a la web de ingreso de cliente
    When  ingresa sus datos
      | 1 |Email Address|Password|
    And presiona el boton de Sign-In
    Then puede ingresar como cliente

  @test-2 @test
  Scenario Outline: Test 2
    Given ingresa a la web de ingreso de cliente
    When  ingresa sus datos "<Email Address>" "<Password>" de la "<row>"
    When presiona el boton de Sign-In
    Then puede ingresar como cliente
    Examples:
      | row | Email Address | Password |
      | 1   | mail | Password |
      | 2   | mail2 | Password |