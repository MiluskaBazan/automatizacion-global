package auts.parquearauco.web.bdd.steps;

import genericos.dataReader.DataReaderFactory;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.util.List;

public class parqueAraucoSteps {

    String csvFile = "\\csv\\my_csv.csv";
    String txtFile = "\\txt\\my_txt.txt";
    String jsonFile = "\\json\\my_json.json";
    String excelFile = "\\excel\\my_excel.xlsx";


    @Given("ingresa como usuario desde el homePage")
    public void ingresaComoUsuarioDesdeElHomePage() {
        System.out.println("ingresa como usuario desde el homePage");
    }

    @When("presiona el boton de Sign-In")
    public void presionaElBotonDeSignIn() {
        System.out.println("presiona el boton de Sign-In");
    }

    @Then("puede ingresar como cliente")
    public void puedeIngresarComoCliente() { System.out.println("puede ingresar como cliente"); }

    @Given("ingresa a la web de ingreso de cliente")
    public void ingresaALaWebDeIngresoDeCliente() { System.out.println("ingresa a la web de ingreso de cliente"); }

    @When("ingresa sus datos")
    public void ingresaSusDatos(DataTable dataTable) throws Throwable {
        List<List<String>> rows = dataTable.asLists(String.class);
        JSONObject data;
        data = DataReaderFactory.getReader("json").readJson(jsonFile,"parquearauco");
        System.out.println("Nombre: "+data.get("nombre"));
        System.out.println("Apellido: "+data.get("apellido"));

        //JsonArray
        JSONArray array = (JSONArray) data.get("address");
        for (int i = 0; i < array.size(); i++) {
            JSONObject direccion = (JSONObject) array.get(i);
            System.out.println("Direccion "+(i+1)+" del Cliente: "+data.get("nombre")+" "+data.get("apellido"));
            System.out.println(direccion.get("street"));
            System.out.println(direccion.get("city"));
            System.out.println(direccion.get("state"));
        }    }

    @When("ingresa sus datos {string} {string} de la {string}")
    public void ingresaSusDatosDeLa(String email, String password, String row) throws Throwable {
        System.out.println("ingreso mis datos {string} {string} de la {string}");
    }

    @Given("Que estoy en la pagina web www.parquearauco.cl")
    public void queEstoyEnLaPaginaWebWwwParquearaucoCl() {

    }

    @When("Busco una tienda o categoría por el buscador")
    public void buscoUnaTiendaOCategoríaPorElBuscador() {

    }

    @Then("Aparece información básica de la tienda buscada")
    public void apareceInformaciónBásicaDeLaTiendaBuscada() {
    }
}
