package genericos.dataReader.readers;


import componentesComunes.dataReader.exceptions.DataReaderException;
import genericos.funciones.LoadProperties;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;

public class JsonReader implements IDataReader{


    public JSONObject readJson(String file, String archivoPropiedades) throws Throwable {

        System.out.println("JsonReader: " +System.getProperty("user.dir")+LoadProperties.loadProperties(archivoPropiedades).getProperty("ruta_test_data") + file);

        try {
            //Parsear el archivo
            JSONParser jsonParser = new JSONParser();
            FileReader reader = new FileReader(System.getProperty("user.dir")+LoadProperties.loadProperties(archivoPropiedades).getProperty("ruta_test_data")+file);

            //guardar en un Objecto para convertir en un Json Object
            Object object = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject)object;
            return jsonObject;
        } catch (Throwable throwable) {
            throw new DataReaderException("Error al abrir el archivo: " + throwable.getMessage());
        }
    }

    @Override
    public List<HashMap<String, String>> readFile(String file) throws Throwable {
        return null;
    }

}
