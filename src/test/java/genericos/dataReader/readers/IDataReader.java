package genericos.dataReader.readers;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.List;

public interface IDataReader {
    JSONObject readJson(String jsonFile, String archivoPropiedades) throws Throwable;
    List<HashMap<String, String>> readFile(String file)  throws Throwable;
}
